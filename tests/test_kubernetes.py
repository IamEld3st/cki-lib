"""Test KubernetesHelper."""
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_lib import kubernetes


class TestKubernetes(unittest.TestCase):
    """Test cki_lib/kubernetes.py."""

    @mock.patch('kubernetes.client.ApiClient')
    def test_api_client(self, api_client):
        """Test the api_client property."""
        helper = kubernetes.KubernetesHelper()
        self.assertEqual(helper.api_client,
                         api_client.return_value)
        api_client.assert_called()

    @mock.patch('kubernetes.client.CoreV1Api')
    def test_api_corev1(self, api_corev1):
        """Test the api_corev1 accessor."""
        helper = kubernetes.KubernetesHelper()
        self.assertEqual(helper.api_corev1(),
                         api_corev1.return_value)
        api_corev1.assert_called()

    @mock.patch.dict(os.environ, {'OPENSHIFT_KEY': 'key',
                                  'OPENSHIFT_SERVER': 'server',
                                  'OPENSHIFT_PROJECT': 'project',
                                  'REQUESTS_CA_BUNDLE': 'cacert'})
    def test_setup_environment(self):
        """Test the configuration from env variables."""
        helper = kubernetes.KubernetesHelper()
        helper.setup()
        self.assertEqual(helper.config.api_key_prefix['authorization'],
                         'Bearer')
        self.assertEqual(helper.config.api_key['authorization'], 'key')
        self.assertEqual(helper.config.host, 'server')
        self.assertEqual(helper.config.ssl_ca_cert, 'cacert')
        self.assertEqual(helper.namespace, 'project')

    @mock.patch.dict(os.environ, {'OPENSHIFT_KEY': 'key',
                                  'OPENSHIFT_SERVER': 'server',
                                  'OPENSHIFT_PROJECT': 'project',
                                  'REQUESTS_CA_BUNDLE': 'cacert'})
    def test_setup_environment_none(self):
        """Test the configuration from env variables for prefix=None."""
        helper = kubernetes.KubernetesHelper()
        helper.setup(env_var_prefix=None)
        self.assertEqual(helper.config.api_key_prefix['authorization'],
                         'Bearer')
        self.assertEqual(helper.config.api_key['authorization'], 'key')
        self.assertEqual(helper.config.host, 'server')
        self.assertEqual(helper.config.ssl_ca_cert, 'cacert')
        self.assertEqual(helper.namespace, 'project')

    @mock.patch.dict(os.environ, {'CUSTOM_KEY': 'key',
                                  'CUSTOM_SERVER': 'server',
                                  'CUSTOM_PROJECT': 'project',
                                  'REQUESTS_CA_BUNDLE': 'cacert'})
    def test_setup_environment_custom(self):
        """Test the configuration from custom env variables."""
        helper = kubernetes.KubernetesHelper()
        helper.setup(env_var_prefix='CUSTOM')
        self.assertEqual(helper.config.api_key_prefix['authorization'],
                         'Bearer')
        self.assertEqual(helper.config.api_key['authorization'], 'key')
        self.assertEqual(helper.config.host, 'server')
        self.assertEqual(helper.namespace, 'project')

    @mock.patch.dict(os.environ, {'KUBERNETES_SERVICE_HOST': 'server',
                                  'KUBERNETES_SERVICE_PORT': 'port'})
    def test_setup_cluster(self):
        """Test the configuration from the in-cluster config."""
        with tempfile.TemporaryDirectory() as directory:
            token_file = pathlib.Path(directory, 'token')
            token_file.write_text('key', encoding='utf8')
            cert_file = pathlib.Path(directory, 'ca.crt')
            cert_file.write_text('cert', encoding='utf8')
            namespace_file = pathlib.Path(directory, 'namespace')
            namespace_file.write_text('project', encoding='utf8')

            with mock.patch('kubernetes.config.incluster_config.SERVICE_TOKEN_FILENAME',
                            str(token_file)), \
                mock.patch('kubernetes.config.incluster_config.SERVICE_CERT_FILENAME',
                           str(cert_file)), \
                mock.patch('cki_lib.kubernetes.KubernetesHelper.cluster_namespace_path',
                           namespace_file):
                helper = kubernetes.KubernetesHelper()
                helper.setup()

        self.assertEqual(helper.config.host, 'https://server:port')
        self.assertEqual(helper.config.api_key['authorization'], 'bearer key')
        self.assertEqual(helper.namespace, 'project')
        self.assertEqual(helper.config.ssl_ca_cert,
                         str(cert_file))

    @mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': 'cacert'})
    @mock.patch('os.path.isdir', mock.Mock(return_value=False))
    @mock.patch('os.path.exists', mock.Mock(return_value=True))
    def test_setup_kube_config(self):
        """Test the configuration from the .kube/config file."""
        mock_open = mock.mock_open(read_data=json.dumps({
            "apiVersion": "v1", "kind": "Config", "current-context": "context",
            "contexts": [{"name": "context",
                          "context": {"cluster": "cluster", "namespace":
                                      "project", "user": "user"}}],
            "clusters": [{"name": "cluster", "cluster": {"server": "server"}}],
            "users": [{"name": "user", "user": {"token": "key"}}]}))

        with mock.patch('kubernetes.config.kube_config.open', mock_open):
            helper = kubernetes.KubernetesHelper()
            helper.setup()
        mock_open.assert_called_with(os.path.expanduser('~/.kube/config'))
        self.assertEqual(helper.config.api_key['authorization'], 'Bearer key')
        self.assertEqual(helper.config.host, 'server')
        self.assertEqual(helper.config.ssl_ca_cert, 'cacert')
        self.assertEqual(helper.namespace, 'project')
