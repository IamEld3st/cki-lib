"""Kubernetes Helper."""
import os
import pathlib

import kubernetes


class KubernetesHelper:
    """Provide some Kubernetes helper functions."""

    cluster_namespace_path = pathlib.Path('/var/run/secrets/kubernetes.io/serviceaccount/namespace')

    def __init__(self):
        """Create a new Kubernetes helper."""
        self.config = kubernetes.client.Configuration()
        self.config_type = None
        self.namespace = None

    def setup(self, env_var_prefix=None):
        """Set up the configuration to allow API calls.

        The configuration will be determined from (in this order):
        - OPENSHIFT* env variables if OPENSHIFT_KEY is defined. The prefix can
          be customized by setting env_var_prefix.
        - the current context from ~/.kube/config if this file exists
        - /var/run/secrets/kubernetes.io/serviceaccount inside the cluster
        """
        env_var_prefix = env_var_prefix or 'OPENSHIFT'
        if f'{env_var_prefix}_KEY' in os.environ:
            self._setup_environment(env_var_prefix)
        elif self.cluster_namespace_path.exists():
            self._setup_cluster()
        else:
            self._setup_kube_config()

    @property
    def api_client(self):
        """Return ApiClient."""
        return kubernetes.client.ApiClient(configuration=self.config)

    def api_corev1(self):
        """Return a Kubernetes CoreV1Api client."""
        return kubernetes.client.CoreV1Api(self.api_client)

    def _setup_environment(self, env_var_prefix):
        self.config_type = f'{env_var_prefix}_* environment variables'
        self.config.host = os.environ[f'{env_var_prefix}_SERVER']
        self.config.ssl_ca_cert = os.environ['REQUESTS_CA_BUNDLE']
        self.config.api_key_prefix['authorization'] = 'Bearer'
        self.config.api_key['authorization'] = os.environ[f'{env_var_prefix}_KEY']
        self.namespace = os.environ[f'{env_var_prefix}_PROJECT']

    def _setup_cluster(self):
        self.config_type = 'cluster configuration'
        kubernetes.config.load_incluster_config(self.config)
        self.namespace = self.cluster_namespace_path.read_text(encoding='utf8')

    def _setup_kube_config(self):
        self.config_type = '~/.kube/config'
        kubernetes.config.load_kube_config(client_configuration=self.config)
        config_contexts = kubernetes.config.list_kube_config_contexts()
        self.config.ssl_ca_cert = os.environ['REQUESTS_CA_BUNDLE']
        self.namespace = config_contexts[1]['context']['namespace']
